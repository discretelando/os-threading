#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "anagram.h"

char* lowerCaser();
void charSorter();
struct StringList *MallocSList(char *word){
	struct StringList *newStringNode = malloc(sizeof(struct StringList));
	newStringNode->Word =strdup(word);
	newStringNode->Next = NULL;
	return newStringNode;
}

void AppendSList(struct StringList **head, struct StringList *node){
	struct StringList *currentNode = *head;
	if((*head)->Word == NULL){
		(*head) = node;
	}
	else{
	while(currentNode->Next != NULL){	
		currentNode = currentNode->Next;
	}
		currentNode->Next = node;
	}
}

void FreeSList(struct StringList **node){
	struct StringList *currentNode = *node;
	struct StringList *tempNode = NULL;
	while(currentNode->Next != NULL){
		tempNode = currentNode->Next;
		free(currentNode->Word);
		free(currentNode->Next);
		free(currentNode);
		currentNode = tempNode;
	}
}

void PrintSList(FILE *file, struct StringList *node){
	int count = 0;
	while(node != NULL){
		if(count == 0)
		fprintf(file, "\t%s", node->Word);
		node = node->Next;
	}
}

int SListCount(struct StringList *node){
	int count = 0;
	while(node->Next != NULL){
		count++;
		node = node->Next;
	}
	return count;
}

struct AnagramList* MallocAList(char *word){
	char newAnagram[100] = "";
	strcpy(newAnagram, word);
	int len = strlen(word); 
	lowerCaser(newAnagram);
	charSorter(newAnagram, 0, len-1);
	struct AnagramList *newAnagramNode = malloc(sizeof(struct AnagramList));
	newAnagramNode->Anagram = strdup(newAnagram);
	newAnagramNode->Next = NULL;
	newAnagramNode->Words = MallocSList(word);
	return newAnagramNode;
}

void FreeAList(struct AnagramList **node){
	struct AnagramList *nextNode = *node;	
	while(nextNode->Next != NULL){
		struct StringList **headNode = &nextNode->Words;
		struct AnagramList *tempNode = nextNode;
		FreeSList(headNode);
		free(nextNode->Words);
		free(nextNode->Next);
		free(nextNode->Anagram);
		free(nextNode);
		nextNode = tempNode->Next;
	} 
}

void PrintAList(FILE *file, struct AnagramList *node){
	while(node != NULL){
		int n = 1+SListCount(node->Words);
		if( n > 1){
		fprintf(file, "%s:%d\n", node->Anagram, n);
		PrintSList(file, node->Words);
		}
		node = node->Next;
	}
}

char* lowerCaser(char *word){
	char newword[100];
	strcpy(newword, word);
	int len = strlen(word);
	for(int i = 0; i < len; i++){
		if(newword[i] >= 65 && newword[i] <= 90){
			//printf("%c\n", newword[i]);
			newword[i]+= 32;
		}
	}
 	//printf("%s\n", newword);
 	strcpy(word, newword);
	return word;
}

static int partition(char *a, int left, int right) //https://stackoverflow.com/questions/1041866/what-is-the-effect-of-extern-c-in-c 
{
    int pivot, i, j, t;
    pivot = a[left];
    i = left;
    j = right + 1;

    while (1)
    {
        do i++; while (a[i] <= pivot && i <= right);
        do j--; while (a[j] > pivot);
        if (i >= j) break;
        t = a[i]; a[i] = a[j]; a[j] = t;
    }
    t = a[left]; a[left] = a[j]; a[j] = t;
    return j;
}

extern void charSorter(char *a, int left, int right) //https://stackoverflow.com/questions/1041866/what-is-the-effect-of-extern-c-in-c
{
    int j;
    if (left < right)
    {
        j = partition(a, left, right); //i changed a to be a char pointer instead of a char array that the example gives from Stack Overflow to match more of my existing code structure
        charSorter(a, left, j - 1);
        charSorter(a, j + 1, right);
    }
}

int stringCheck(char *word){
	int length = strlen(word);
	for(int i = 0; i < length; i++){
		if(word[i] != 10 && word[i] < 65 || word[i] > 90){
			if(word[i] != 10 && word[i] < 97 || word[i] > 122){
				return 0;
			}
		}
	}
	return 1;
}

void AddWordAList(struct AnagramList **node, char *word){
	char newAnagram[100] = "";
	strcpy(newAnagram, word);
	int len = strlen(word); 
	lowerCaser(newAnagram);
	charSorter(newAnagram, 0, len-1);
	struct AnagramList *currentNode = *node;

	if(currentNode == NULL){
			struct AnagramList *newAnagramNode = MallocAList(word);
			*node = newAnagramNode;
	}
	else{
		if(strcmp(currentNode->Anagram, newAnagram) == 0){
			AppendSList(&currentNode->Words, MallocSList(word));
			return;
		}

		while(currentNode->Next != NULL){
		if(strcmp(currentNode->Anagram, newAnagram) == 0){
			AppendSList(&currentNode->Words, MallocSList(word));
			return;
		}
		currentNode = currentNode->Next;
	}
		if(strcmp(currentNode->Anagram, newAnagram) == 0){
		AppendSList(&currentNode->Words, MallocSList(word));
		return;
	}
			struct AnagramList *newAnagramNode = MallocAList(word);
			currentNode->Next = newAnagramNode;
	}
}
