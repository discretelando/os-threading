#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "anagram.h"
#define MAX_BUFF 100

int stringCheck();
char *lowerCaser();
void charSorter(); 

int main(int argc, char *argv[]){
	FILE *infile;
	FILE *outfile;
	infile = stdin;
	outfile = stdout;
	char input[MAX_BUFF];

	if(argc == 1){
		if(infile == NULL || outfile == NULL){
			printf("Error opening file\n");
			exit(0);
		}
	if(fgets(input, MAX_BUFF, infile) !=  NULL)
		{
			struct AnagramList *head = NULL;
			if(stringCheck(input) != 1){
				printf("Invalid Word Entry\n");
			}
			else{
				AddWordAList(&head, input);
			}

		while(fgets(input, MAX_BUFF, infile) != NULL){
			if(stringCheck(input) != 1){
				printf("Invalid Word Entry\n");
			}
			else{
				AddWordAList(&head, input);
			}
		}
		PrintAList(outfile, head);
	}
}

	else if(argc == 2){
		infile = fopen(argv[1], "r");
		if(infile == NULL || outfile == NULL){
			printf("Error opening file\n");
			exit(0);
		}
		if(fgets(input, MAX_BUFF, infile) !=  NULL)
		{
			struct AnagramList *head = NULL;
			if(stringCheck(input) != 1){
				printf("Invalid Word Entry\n");
			}
			else{
				AddWordAList(&head, input);
			}

		while(fgets(input, MAX_BUFF, infile) != NULL){
			if(stringCheck(input) != 1){
				printf("Invalid Word Entry\n");
			}
			else{
				AddWordAList(&head, input);
			}
		}
		PrintAList(outfile, head);
		//FreeAList(newhead);
	}
}


	//read from file write to stdout
	else if(argc == 3){
		infile = fopen(argv[1], "r");
		outfile = fopen(argv[2], "w");
		if(infile == NULL || outfile == NULL){
			printf("Error opening file\n");
			exit(0);
		}
		if(fgets(input, MAX_BUFF, infile) !=  NULL)
		{
			struct AnagramList *head = NULL;
			if(stringCheck(input) != 1){
				printf("Invalid Word Entry\n");
			}
			else{
				AddWordAList(&head, input);
			}

		while(fgets(input, MAX_BUFF, infile) != NULL){
			if(stringCheck(input) != 1){
				printf("Invalid Word Entry\n");
			}
			else{
				AddWordAList(&head, input);
			}
		}
		PrintAList(outfile, head);
		//FreeAList(newhead);
		}
	}
	return 0;
}