WOW! What a journey.
This program has put me through the ringer. But I finally got it to work and I couldn't be more proud.
I honestly don't have the mental fortitude to tackle getting through all of my valgrind issues. However, there are no GDB issues that I am aware of. Valgrind complained about possibly lost data but little definitely lost. 

I think this project is about 80 to 90% percent complete with that information being said. I believe my code is pretty sturdy I implemented a few checks to ensure that only valid words can be processed. I did not get to vet all possible scenarios, so I'm sure Roscoe will have fun ripping apart my code. I'm crossing my fingers in hopes that atleast there won't be any seg fault errors. 

Aside from that I am pretty proud of the work that I have done. I think I would have been able to polish it a lot more had I started maybe a few days earlier. ALl in all I enjoyed this project and have already started on Project 1 which will be more of a hassle.
