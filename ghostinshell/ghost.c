#include<stdio.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include "ghost.h"
#define MAX_BUFF 100

 void PrintProcess(FILE *outfile, struct ProcessControlBlock * process){
 }

 struct ProcessControlBlock** MallocProcessControlBlock(FILE *infile){
 	char input[MAX_BUFF] = "";
 	int count = 0;
	while(fgets(input, MAX_BUFF, infile) != NULL){
		count++;
		//token = strtok(input, " ");
	}

	fseek(infile, 0, SEEK_SET);
	struct ProcessControlBlock **PCBS = malloc(count * sizeof(struct ProcessControlBlock*));
	for(int i = 0; i < count; i++){
		PCBS[i] = malloc(sizeof(struct ProcessControlBlock));
		fgets(input, MAX_BUFF, infile);
		PCBS[i]->CMD = malloc(sizeof(struct ProcessControlBlock));
		PCBS[i]->CMD_ARGS = NULL;
		PCBS[i]->PID = 0;
		char* arguments[50];
		char* token = malloc(sizeof(strlen(input) + 1));
		token = strtok(input,"  \n");
		strcpy(PCBS[i]->CMD,token);
		int argcount = 0;
		while(token != NULL){
			token = strtok(NULL, " \n");
			if(token != NULL){
				arguments[argcount] = token;
				argcount++;
			}
		}
		PCBS[i]->CMD_ARGS = malloc(argcount * sizeof(char*));
		for(int z = 0; z < argcount; z++){
			PCBS[i]-> CMD_ARGS[z] = malloc(sizeof(strlen(arguments[z]) + 1));
			strcpy(PCBS[i]->CMD_ARGS[z], arguments[z]);
		}
		PCBS[i]->count_args = argcount;
		arguments[argcount] = NULL;
	}
	// if(fgets(input, MAX_BUFF, infile) !=  NULL){
	// 		struct AnagramList *head = NULL;
	// 		if(stringCheck(input) != 1){
	// 			printf("Invalid Word Entry\n");
	// 		}
	// 		else{
	// 			AddWordAList(&head, input);
	// 		}

	// 	while(fgets(input, MAX_BUFF, infile) != NULL){
	// 		if(stringCheck(input) != 1){
	// 			printf("Invalid Word Entry\n");
	// 		}
	// 		else{
	// 			AddWordAList(&head, input);
	// 		}
	// 	}
	// }
	return PCBS;
 }

void LaunchAllPrograms(struct ProcessControlBlock** PCBS, int count){
	printf("%d\n", count);
	for(int i = 0; i < count; i++){
		PCBS[i]->PID = fork();
		if(PCBS[i]->PID < 0){
			printf("Encountered an error running program. Exiting program\n");
			exit(1);
		}
		else if(PCBS[i]->PID == 0){
			if(execvp(PCBS[i]->CMD, PCBS[i]->CMD_ARGS) < 0){
				printf("Child Process: %d encountered an error. Exiting\n", PCBS[i]->PID);
				exit(1);
			}
		}
		else{
			wait(&PCBS[i]->PID);
			printf("Child Process %d has exited\n", PCBS[i]->PID);
		}
	}
	//PCBS[i] = fork();
	// if (pid[i] < 0) {
	// 	// handle the error appropriately;
	// }
	// if (pid[i] == 0) {
	// 	execvp(program[i], arguments[i]);
	// 	// log error. starting program failed.
	// 	// exit so duplicate copy of MCP doesn’t run/fork bomb.
	// 	exit(-1);
	// 	}
	// for i=0, numprograms-1 {
	// 	wait(pid[i]);
	// }
}