#ifndef GHOST_H
#define GHOST_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef enum {NOTSTARTED, RUNNING, PAUSED, TERMINATED} STATES;
struct ProcessControlBlock
{
	char *CMD; //string that will hold the command for each process
	char **CMD_ARGS; //collection of strings that will hold each argument related to a command
	int PID; //the process ID returned from using fork()
	int count_args;
	int QUANTUM;
	STATES STATE;
	int HasExited;
	int CPUTIME;
	int IOTIME;
};

struct ProcessControlBlock** MallocProcessControlBlock(); //creates an array of commands on the heap based on workload

void LaunchAllPrograms(struct ProcessControlBlock** procs, int count); //reads the input file and loads it into memory

void PrintProcess(FILE *file, struct ProcessControlBlock* process); //grabs a process and ccprints the cmd and its args

#endif