#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<signal.h>
#include<pthread.h>
#include<sys/wait.h>
#include "ghost.h"
#define MAX_BUFF 100

int RUN = 0;
sigset_t set, oldset;

struct ProcessControlBlock** MallocProcessControlBlock(FILE *infile){
 	char input[MAX_BUFF] = "";
 	int count = 0;
	while(fgets(input, MAX_BUFF, infile) != NULL){
		count++;
		//token = strtok(input, " ");
	}

	fseek(infile, 0, SEEK_SET);
	struct ProcessControlBlock **PCBS = malloc(count * sizeof(struct ProcessControlBlock*));
	for(int i = 0; i < count; i++){
		PCBS[i] = malloc(sizeof(struct ProcessControlBlock));
		fgets(input, MAX_BUFF, infile);
		PCBS[i]->CMD = malloc(sizeof(struct ProcessControlBlock));
		PCBS[i]->CMD_ARGS = NULL;
		PCBS[i]->PID = 0;
		char* arguments[50];
		char* token = "";
		token = strtok(input,"  \n");
		strcpy(PCBS[i]->CMD,token);
		int argcount = 0;
		while(token != NULL){
			token = strtok(NULL, " \n");
			if(token != NULL){
				arguments[argcount] = token;
				argcount++;
			}
		}
		PCBS[i]->CMD_ARGS = malloc(argcount * sizeof(char*));
		for(int z = 0; z < argcount; z++){
			PCBS[i]-> CMD_ARGS[z] = malloc(sizeof(strlen(arguments[z]) + 1));
			strcpy(PCBS[i]->CMD_ARGS[z], arguments[z]);
		}
		PCBS[i]->count_args = argcount;
		arguments[argcount] = NULL;
	}
	fclose(infile);
	return PCBS;
 }

void LaunchAllPrograms(struct ProcessControlBlock** PCBS, int count){
	for(int i = 0; i < count; i++){
		PCBS[i]->PID = fork();
		if(PCBS[i]->PID < 0){
			printf("Encountered an error running program. Exiting program\n");
			exit(-1);
		}
		else if(PCBS[i]->PID == 0){
 			while (!RUN)
 				usleep(500);
 			printf("continued\n");
			if(execvp(PCBS[i]->CMD, PCBS[i]->CMD_ARGS) < 0){
				printf("Child Process: %d encountered an error. Exiting\n", PCBS[i]->PID);
				exit(-1);
			}
		}
		else{
			printf("Child Process %d is waiting\n", PCBS[i]->PID);
		}
	}
}

void FreeShit(struct ProcessControlBlock** PCBS, int COUNT){
    for(int i = 0; i < COUNT; i++){
    	int arc = 0;
    	while(PCBS[i]->CMD_ARGS[arc] != NULL){
    		free(PCBS[i]->CMD_ARGS[arc]);
    		arc++;
    	}
    	free(PCBS[i]->CMD);
    	free(PCBS[i]->CMD_ARGS);
    	free(PCBS[i]);
    }
    free(PCBS);
    printf("These bytes are all free and shall never know pain again\n");
}

void SignalHandler(int sig){
	RUN = 1;
}

void SendAllPrograms(struct ProcessControlBlock** PCBS, int count, int signal){
	for(int i = 0; i < count; i++){
		switch(signal){
			case SIGUSR1:
				sleep(1);
				kill(PCBS[i]->PID, SIGUSR1);
				printf("Signaled: %d\n", PCBS[i]->PID);
				break;
			case SIGCONT:
				kill(PCBS[i]->PID, SIGCONT);
				printf("Continued: %d\n", PCBS[i]->PID);
				break;
			case SIGSTOP:
				kill(PCBS[i]->PID, SIGSTOP);
				printf("Paused: %d\n", PCBS[i]->PID);
				break;
		}
	}
}

void WaitForAllProgramsExit(struct ProcessControlBlock** PCBS, int count){
	for(int i = 0; i < count; i++){
		waitpid(PCBS[i]->PID, NULL, 0);
	}
}

int main(int argc, char* argv[]){
	if(argc != 2){
		printf("Invalid Call");
		exit(-1);
	}
 	FILE * infile = fopen(argv[1], "r");
 	if(infile == NULL){
		printf("Error opening file\n");
		exit(-1);
	}
    char *file_name = argv[1]; 
    char input[MAX_BUFF] = "";	
    int count = 0;
    sigemptyset(&set);
    sigaddset(&set, SIGUSR1);
    signal(SIGUSR1, SignalHandler);
	while(fgets(input, MAX_BUFF, infile) != NULL){
		count++;
	}
	fseek(infile, 0, SEEK_SET);
    struct ProcessControlBlock **PCBS = MallocProcessControlBlock(infile);
    LaunchAllPrograms(PCBS, count);
    SendAllPrograms(PCBS, count, SIGUSR1);
    SendAllPrograms(PCBS, count, SIGSTOP);
    SendAllPrograms(PCBS, count, SIGCONT);
    WaitForAllProgramsExit(PCBS, count);
    FreeShit(PCBS, count);
}