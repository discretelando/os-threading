#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<signal.h>
#include<pthread.h>
#include<sys/wait.h>
#include "ghost.h"
#define MAX_BUFF 100

int AllProgramsHaveExited();
int RUN = 0;
int COUNT = 0;
int CURRENT = 0;
int EXIT = 0;
struct ProcessControlBlock **PCBS = NULL;
sigset_t set, oldset;

struct ProcessControlBlock** MallocProcessControlBlock(FILE *infile){
 	char input[MAX_BUFF] = "";
	while(fgets(input, MAX_BUFF, infile) != NULL){
		COUNT++;
	}

	fseek(infile, 0, SEEK_SET);
	PCBS = malloc(COUNT * sizeof(struct ProcessControlBlock*));
	for(int i = 0; i < COUNT; i++){
		PCBS[i] = malloc(sizeof(struct ProcessControlBlock));
		fgets(input, MAX_BUFF, infile);
		PCBS[i]->CMD = malloc(sizeof(strlen(input)));
		PCBS[i]->CMD_ARGS = NULL;
		PCBS[i]->PID = 0;
		PCBS[i]->HasExited = 0;
		PCBS[i]->STATE = NOTSTARTED;
		char* arguments[50];
		char* token = "";
		token = strtok(input,"  \n");
		strcpy(PCBS[i]->CMD,token);
		int argCOUNT = 0;
		while(token != NULL){
			token = strtok(NULL, " \n");
			if(token != NULL){
				arguments[argCOUNT] = token;
				argCOUNT++;
			}
		}
		PCBS[i]->CMD_ARGS = malloc(argCOUNT * sizeof(char*));
		for(int z = 0; z < argCOUNT; z++){
			PCBS[i]-> CMD_ARGS[z] = malloc(sizeof(strlen(arguments[z]) + 1));
			strcpy(PCBS[i]->CMD_ARGS[z], arguments[z]);
		}
		PCBS[i]->count_args = argCOUNT;
		arguments[argCOUNT] = NULL;
	}
	fclose(infile);
	return PCBS;
 }

void LaunchAllPrograms(struct ProcessControlBlock** PCBS, int COUNT){
	for(int i = 0; i < COUNT; i++){
		PCBS[i]->PID = fork();
		if(PCBS[i]->PID < 0){
			printf("Encountered an error running program. Exiting program\n");
			exit(-1);
		}
		else if(PCBS[i]->PID == 0){
 			while (!RUN)
 				usleep(500);
 			printf("continued\n");
			if(execvp(PCBS[i]->CMD, PCBS[i]->CMD_ARGS) < 0){
				printf("Child Process: %d encountered an error. Exiting\n", PCBS[i]->PID);
				exit(-1);
			}
		}
		else{
			printf("Child Process %d is waiting\n", PCBS[i]->PID);
		}
	}
}

void FreeShit(struct ProcessControlBlock** PCBS, int COUNT){
    for(int i = 0; i < COUNT; i++){
    	int arc = 0;
    	while(PCBS[i]->CMD_ARGS[arc] != NULL){
    		free(PCBS[i]->CMD_ARGS[arc]);
    		arc++;
    	}
    	free(PCBS[i]->CMD);
    	free(PCBS[i]->CMD_ARGS);
    	free(PCBS[i]);
    }
    free(PCBS);
    printf("These bytes are all free and shall never know pain again\n");
}

void SigUsr1Handler(int sig){
	RUN = 1;
}

void SigUsr2Handler(int sig){
	EXIT = 1;
}
void AlarmHandler(int sig){
	raise(SIGCHLD);
	printf("alarm received\n");
	if(PCBS[CURRENT]->HasExited == 1){
		PCBS[CURRENT]->STATE = TERMINATED;
		printf("Process %d: Terminated\n", PCBS[CURRENT]->PID);
	}
	else if(PCBS[CURRENT]->STATE == RUNNING){
		kill(PCBS[CURRENT]->PID, SIGSTOP);
		PCBS[CURRENT]->STATE = PAUSED;
		printf("Process %d: Paused\n", PCBS[CURRENT]->PID);
	}

	if(AllProgramsHaveExited()){
		printf("\n\n\n\nWe're all done LADS...Exiting\n\n");
		raise(SIGUSR2);
	}
	else{
		int compare = CURRENT+1;
		while(1){
			if(compare < (COUNT-1)){
				if(PCBS[compare]->STATE == PAUSED || PCBS[compare]->STATE == NOTSTARTED){
					CURRENT = compare;
					break;
				}
				else
					compare++;
			}
			else if(compare == (COUNT-1)){
				if(PCBS[compare]->STATE == PAUSED || PCBS[compare]->STATE == NOTSTARTED){
					CURRENT = compare;
					break;
				}
				else{
					compare = 0;
				}
			}
			else
				compare = 0;
		}
		//printf("current:%d\n", CURRENT);
		if(PCBS[CURRENT]->STATE == NOTSTARTED){
			kill(PCBS[CURRENT]->PID, SIGUSR1);
			alarm(1);
			PCBS[CURRENT]->STATE = RUNNING;
			printf("Process %d: Started\n", PCBS[CURRENT]->PID);
		}
		else if(PCBS[CURRENT]->STATE == PAUSED){
			kill(PCBS[CURRENT]->PID, SIGCONT);
			alarm(1);
			printf("Process %d: Running\n", PCBS[CURRENT]->PID);
			PCBS[CURRENT]->STATE = RUNNING;
		}
	}
}

void ChildHandler(int sig){
	for(int i =0; i < COUNT; i++){
		int status;
		if(waitpid(PCBS[i]->PID,&status,WNOHANG) > 0 )   // the call to get a status update worked, WNOHANG specifies async call, it doesn't NOT wait.
   			if(WIFEXITED(status))   // Details under waitpid, this is a macro/function to extract status codes.
				PCBS[i]->HasExited = 1;
	}
}

void SendAllPrograms(struct ProcessControlBlock** PCBS, int COUNT, int signal){
	for(int i = 0; i < COUNT; i++){
		switch(signal){
			case SIGUSR1:
				sleep(1);
				kill(PCBS[i]->PID, SIGUSR1);
				printf("Signaled: %d\n", PCBS[i]->PID);
				break;
			case SIGCONT:
				kill(PCBS[i]->PID, SIGCONT);
				printf("Continued: %d\n", PCBS[i]->PID);
				break;
			case SIGSTOP:
				kill(PCBS[i]->PID, SIGSTOP);
				printf("Paused: %d\n", PCBS[i]->PID);
				break;
		}
	}
}
void PleaseWait(int sig){
	sigset_t set;
	int signal = 0;
    sigemptyset(&set);
    sigaddset(&set, sig);
    sigwait(&set, &signal);
}
void WaitForAllProgramsExit(struct ProcessControlBlock** PCBS, int COUNT){
	while(!EXIT)
		usleep(1000);
}

int AllProgramsHaveExited(){
	for(int i = 0; i < COUNT; i++){
		if(PCBS[i]->HasExited == 0){
			return 0;
		}
	}
	return 1;
}

int main(int argc, char* argv[]){
	if(argc != 2){
		printf("Invalid Call");
		exit(-1);
	}
 	FILE * infile = fopen(argv[1], "r");
 	if(infile == NULL){
		printf("Error opening file\n");
		exit(-1);
	}
    char *file_name = argv[1]; 
    char input[MAX_BUFF] = "";	
    int COUNT = 0;
    signal(SIGUSR1, SigUsr1Handler);
    signal(SIGUSR2, SigUsr2Handler);
    signal(SIGALRM, AlarmHandler);
    signal(SIGCHLD, ChildHandler);
	while(fgets(input, MAX_BUFF, infile) != NULL){
		COUNT++;
	}
	fseek(infile, 0, SEEK_SET);
    struct ProcessControlBlock **PCBS = MallocProcessControlBlock(infile);
    LaunchAllPrograms(PCBS, COUNT);
    printf("alarm\n");
    alarm(1);
    WaitForAllProgramsExit(PCBS, COUNT);
    FreeShit(PCBS, COUNT);
}