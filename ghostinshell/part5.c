#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<signal.h>
#include<pthread.h>
#include<sys/wait.h>
#include "ghost.h"
#define MAX_BUFF 300

int AllProgramsHaveExited();
void ProgramStatistics();
void UpdateQuantum();
int RUN = 0;
int COUNT = 0;
int CURRENT = 0;
int EXIT = 0;
struct ProcessControlBlock **PCBS = NULL;
sigset_t set, oldset;

struct ProcessControlBlock** MallocProcessControlBlock(FILE *infile){
 	char input[MAX_BUFF] = "";
	while(fgets(input, MAX_BUFF, infile) != NULL){
		COUNT++;
	}

	fseek(infile, 0, SEEK_SET);
	PCBS = malloc(COUNT * sizeof(struct ProcessControlBlock*));
	for(int i = 0; i < COUNT; i++){
		PCBS[i] = malloc(sizeof(struct ProcessControlBlock));
		fgets(input, MAX_BUFF, infile);
		PCBS[i]->CMD = malloc(sizeof(strlen(input)));
		PCBS[i]->CMD_ARGS = NULL;
		PCBS[i]->PID = 0;
		PCBS[i]->HasExited = 0;
		PCBS[i]->STATE = NOTSTARTED;
		PCBS[i]->IOTIME = 0;
		PCBS[i]->CPUTIME = 0;
		char* arguments[50];
		char* token = "";
		token = strtok(input,"  \n");
		strcpy(PCBS[i]->CMD,token);
		int argcount = 0;
		while(token != NULL){
			token = strtok(NULL, " \n");
			if(token != NULL){
				arguments[argcount] = token;
				argcount++;
			}
		}
		PCBS[i]->CMD_ARGS = malloc(argcount * sizeof(char*));
		for(int z = 0; z < argcount; z++){
			PCBS[i]-> CMD_ARGS[z] = malloc(sizeof(strlen(arguments[z]) + 1));
			strcpy(PCBS[i]->CMD_ARGS[z], arguments[z]);
		}
		PCBS[i]->count_args = argcount;
		arguments[argcount] = NULL;
		//free(token);
	}
	return PCBS;
 }

void LaunchAllPrograms(struct ProcessControlBlock** PCBS, int COUNT){
	for(int i = 0; i < COUNT; i++){
		PCBS[i]->PID = fork();
		if(PCBS[i]->PID < 0){
			printf("Encountered an error running program. Exiting program\n");
			exit(-1);
		}
		else if(PCBS[i]->PID == 0){
 			while (!RUN)
 				usleep(500);
 			printf("continued\n");
			if(execvp(PCBS[i]->CMD, PCBS[i]->CMD_ARGS) < 0){
				printf("Child Process: %d encountered an error. Exiting\n", PCBS[i]->PID);
				exit(-1);
			}
		}
		else{
			printf("Child Process %d is waiting\n", PCBS[i]->PID);
		}
	}
}

void FreeShit(struct ProcessControlBlock** PCBS, int COUNT){
    for(int i = 0; i < COUNT; i++){
    	int arc = 0;
    	free(PCBS[i]->CMD);
    	while(PCBS[i]->CMD_ARGS[arc] != NULL){
    		free(PCBS[i]->CMD_ARGS[arc]);
    		arc++;
    	}
    	free(PCBS[i]->CMD_ARGS);
    	free(PCBS[i]);
    }
    free(PCBS);
    printf("These bytes are all free and shall never know pain again\n");
}

void SigUsr1Handler(int sig){
	RUN = 1;
}

void SigUsr2Handler(int sig){
	EXIT = 1;
}
void AlarmHandler(int sig){
	raise(SIGCHLD);
	printf("alarm received\n");
	if(PCBS[CURRENT]->HasExited == 1){
		PCBS[CURRENT]->STATE = TERMINATED;
		printf("Process %d: Terminated\n", PCBS[CURRENT]->PID);
	}
	else if(PCBS[CURRENT]->STATE == RUNNING){
		kill(PCBS[CURRENT]->PID, SIGSTOP);
		PCBS[CURRENT]->STATE = PAUSED;
		printf("Process %d: Paused\n", PCBS[CURRENT]->PID);
	}

	if(AllProgramsHaveExited()){
		printf("\n\n\n\nWe're all done LADS...Exiting\n\n");
		raise(SIGUSR2);
	}
	else{
		int compare = CURRENT+1;
		while(1){
			if(compare < (COUNT-1)){
				if(PCBS[compare]->STATE == PAUSED || PCBS[compare]->STATE == NOTSTARTED){
					CURRENT = compare;
					break;
				}
				else
					compare++;
			}
			else if(compare == (COUNT-1)){
				if(PCBS[compare]->STATE == PAUSED || PCBS[compare]->STATE == NOTSTARTED){
					CURRENT = compare;
					break;
				}
				else{
					compare = 0;
				}
			}
			else
				compare = 0;
		}
		UpdateQuantum();
		if(PCBS[CURRENT]->STATE == NOTSTARTED){
			kill(PCBS[CURRENT]->PID, SIGUSR1);
			alarm(PCBS[CURRENT]->QUANTUM);
			PCBS[CURRENT]->STATE = RUNNING;
			printf("Process %d: Started\n", PCBS[CURRENT]->PID);
		}
		else if(PCBS[CURRENT]->STATE == PAUSED){
			kill(PCBS[CURRENT]->PID, SIGCONT);
			alarm(PCBS[CURRENT]->QUANTUM);
			printf("Process %d: Running\n", PCBS[CURRENT]->PID);
			PCBS[CURRENT]->STATE = RUNNING;
		}
	}
}

void ChildHandler(int sig){
	for(int i =0; i < COUNT; i++){
		int status;
		if(waitpid(PCBS[i]->PID,&status,WNOHANG) > 0 )   // the call to get a status update worked, WNOHANG specifies async call, it doesn't NOT wait.
   			if(WIFEXITED(status))   // Details under waitpid, this is a macro/function to extract status codes.
				PCBS[i]->HasExited = 1;
	}
}

void SendAllPrograms(struct ProcessControlBlock** PCBS, int COUNT, int signal){
	for(int i = 0; i < COUNT; i++){
		switch(signal){
			case SIGUSR1:
				sleep(1);
				kill(PCBS[i]->PID, SIGUSR1);
				printf("Signaled: %d\n", PCBS[i]->PID);
				break;
			case SIGCONT:
				kill(PCBS[i]->PID, SIGCONT);
				printf("Continued: %d\n", PCBS[i]->PID);
				break;
			case SIGSTOP:
				kill(PCBS[i]->PID, SIGSTOP);
				printf("Paused: %d\n", PCBS[i]->PID);
				break;
		}
	}
}
void PleaseWait(int sig){
	sigset_t set;
	int signal = 0;
    sigemptyset(&set);
    sigaddset(&set, sig);
    sigwait(&set, &signal);
}
void WaitForAllProgramsExit(struct ProcessControlBlock** PCBS, int COUNT){
	while(!EXIT)
		usleep(1000);
}

int AllProgramsHaveExited(){
	for(int i = 0; i < COUNT; i++){
		if(PCBS[i]->HasExited == 0){
			return 0;
		}
	}
	return 1;
}

void UpdateQuantum(){
	int io = PCBS[CURRENT]->IOTIME;
	int cpu = PCBS[CURRENT]->CPUTIME;
	if(io > (cpu*.25)){
		PCBS[CURRENT]->QUANTUM = 1;
	}
	else{
		PCBS[CURRENT]->QUANTUM = 5;
	}
}
void ProgramStatistics(){
	while(!EXIT){
	sleep(1);
	for(int i = 0; i < COUNT; i++){
		int track = 0;
		char count[MAX_BUFF];
		char file[MAX_BUFF] = "/proc/";
		char stats_output[MAX_BUFF] = "";
		char* finalout[50];
		sprintf(count, "%d", PCBS[i]->PID);
		strcat(file,count);
		strcpy(count, "/stat");
		strcat(file,count);
		FILE *procfile = fopen(file, "r");
		if(procfile == NULL)
			continue;
		if(fgets(stats_output, MAX_BUFF, procfile) != NULL){
			char* token = "";
			token = strtok(stats_output,"  \n");
			while(token != NULL){
				track+=1;
				if(track == 1){
					finalout[0] = token;
				}

				token = strtok(NULL, " \n");
				if(token != NULL){
					if(track == 3){
						finalout[1] = token;
					}
					else if(track == 13){
						finalout[2] = token;
						PCBS[i]->IOTIME = atoi(token);
					}
					else if(track == 14){
						finalout[3] = token;
						PCBS[i]->CPUTIME = atoi(token);		
					}
				}
			}
			printf("PID: %s   ParentPID: %s   IO: %s   Kernel: %s\n", finalout[0], finalout[1], finalout[2],finalout[3]);			
		}
		fclose(procfile);
	}
 }
}
int main(int argc, char* argv[]){
	if(argc != 2){
		printf("Invalid Call");
		exit(-1);
	}
 	FILE * infile = fopen(argv[1], "r");
 	if(infile == NULL){
		printf("Error opening file\n");
		exit(-1);
	}
    char *file_name = argv[1]; 
    char input[MAX_BUFF] = "";	
    int COUNT = 0;
    signal(SIGUSR1, SigUsr1Handler);
    signal(SIGUSR2, SigUsr2Handler);
    signal(SIGALRM, AlarmHandler);
    signal(SIGCHLD, ChildHandler);
	while(fgets(input, MAX_BUFF, infile) != NULL){
		COUNT++;
	}
	fseek(infile, 0, SEEK_SET);
    struct ProcessControlBlock **PCBS = MallocProcessControlBlock(infile);
   	fclose(infile);
    LaunchAllPrograms(PCBS, COUNT);
    alarm(1);
    ProgramStatistics();
    WaitForAllProgramsExit(PCBS, COUNT);
    FreeShit(PCBS, COUNT);
}