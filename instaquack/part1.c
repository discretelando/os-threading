/*
This file serves as the outline for the Topic Queue
Implements the Thread Safe Queue header files to help maintain
A circular bounded buffer
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "thread_structs.h"
#include "thread_safe_bounded_queue.h"
#include <sys/time.h>
#define QUACKSIZE 500
#define	CAPTIONSIZE 1000

TSBoundedQueue *MallocTopicQueue(long size){
	struct thread_safe_bounded_queue *returnValue = NULL;
	returnValue = TS_BB_MallocBoundedQueue(size);
	return (TSBoundedQueue *)returnValue;
}

long long TryAddEntry(struct thread_safe_bounded_queue *queue, void *item){
	long long returnValue = TS_BB_TryEnqueue(queue,item);
	return returnValue;
}

int Thread_TryDequeue(struct thread_safe_bounded_queue *queue,long long id)
{
        int  returnValue = TS_BB_TryDequeue(queue, id); 
        return returnValue;
}

long long Thread_GetFront(struct thread_safe_bounded_queue *queue)
{
        long long returnValue = TS_BB_GetFront(queue);
        return returnValue;
}

long long Thread_GetBack(struct thread_safe_bounded_queue *queue)
{
        long long returnValue = TS_BB_GetBack(queue);
        return returnValue;
}

int Thread_GetCount(struct thread_safe_bounded_queue *queue)
{
	long long returnValue = TS_BB_GetCount(queue);
	return returnValue;
}

int Thread_IsIdValid(struct thread_safe_bounded_queue *queue,long long id)
{
        int returnValue = TS_BB_IsIdValid(queue, id);
        return returnValue;
}

void *Thread_GetItem(struct thread_safe_bounded_queue *queue,long long id)
{
        void *returnValue = TS_BB_GetItem(queue,id);
        return returnValue;
}

int Thread_IsFull(struct thread_safe_bounded_queue *queue)
{
        int returnValue = TS_BB_IsFull(queue); 
        return returnValue;
}

int Thread_IsEmpty(struct thread_safe_bounded_queue *queue)
{
        int returnValue = TS_BB_IsEmpty(queue);
        return returnValue;
}

void Thread_FreeBoundedQueue(struct thread_safe_bounded_queue *queue)
{
	TS_BB_FreeBoundedQueue(queue);
}

Entry *MakeEntry(int num){
	struct thread_safe_topic_entry *returnValue = NULL;
	returnValue = (struct thread_safe_topic_entry*)malloc(sizeof(struct thread_safe_topic_entry));
	returnValue->entrynum = num;
	returnValue->timestamp = NULL;
	returnValue->pubID = 0;
	returnValue->photoURL[0] = '\0';
	returnValue->photoCaption[0] = '\0';

	return returnValue;
}