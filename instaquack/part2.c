#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <pthread.h>
#include "utilities.h"
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"

// Declaration of thread condition variable 
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;  
  
// declaring mutex 
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER; 

void *Subscriber(void * arg){
  pthread_mutex_lock(&lock);
  pthread_cond_wait(&cond1, &lock);
  printf("%s\n", (char*)arg);
  printf("In function Subscriber thread id = %ld\n", pthread_self());
  pthread_mutex_unlock(&lock); 
}

void *Publisher(void * arg){
  pthread_mutex_lock(&lock);
  pthread_cond_wait(&cond1, &lock);
  printf("%s\n", (char*)arg);
  printf("In function Publisher thread id = %ld\n", pthread_self());
  pthread_mutex_unlock(&lock); 
}

void *Cleanup(void * arg){
  pthread_mutex_lock(&lock);
  pthread_cond_wait(&cond1, &lock);
  printf("%s\n", (char*)arg);
  printf("In function Cleanup thread id = %ld\n", pthread_self());
  pthread_mutex_unlock(&lock); 
}

int main(int argc, char *argv[]){

  // Initialize variables
  pthread_t subscriber, publisher, cleanup;
  char * args = "Hello This Is Subscriber\n";
  char * argp = "Hello This Is Publisher!\n";
  char * argcl = "Hello This Is Cleanup!\n";
  int subscriber_ret = pthread_create(&subscriber, NULL, Subscriber, (void*)args);
  int publisher_ret = pthread_create(&publisher, NULL, Publisher, (void*)argp);
  int cleanup_ret = pthread_create(&cleanup, NULL, Cleanup, (void*)argcl);

  sleep(4);
  int check = 0;
  for(int i = 0; i < 20; i++)
    printf("*****");
  printf("\n");
  pthread_cond_broadcast(&cond1);
  //Join Threads
  check += pthread_join(subscriber, NULL);
  check += pthread_join(publisher, NULL);
  check += pthread_join(cleanup, NULL);

  if(check == 0)
    printf("\n\nALL THREADS JOINED\n");

  for(int i = 0; i < 20; i++)
    printf("*****");
  printf("\n");


  return 0;
}

