#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<pthread.h>
#include<unistd.h>
#include<pthread.h>
#include "utilities.h"
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"
#include "thread_structs.h"
#include "p1fxns.h"
#define THREADSIZE 20
#define QUEUESIZE 20

int DELTA = 0;

// Declaration of thread condition variable 
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;  
  
// declaring mutex 
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

struct arg_struct{
	int index;
	char *filename;
};

void *Subscriber(void * args){
  struct arg_struct *arg = args;
  pthread_mutex_lock(&lock);
  pthread_cond_wait(&cond1, &lock);
  printf("subscriber thread %d reading %s\n", arg->index, (char*)arg->filename);
  printf("In function Subscriber thread id = %ld\n", pthread_self());
  pthread_mutex_unlock(&lock); 
}

void *Publisher(void * args){
  struct arg_struct *arg = args;
  pthread_mutex_lock(&lock);
  pthread_cond_wait(&cond1, &lock);
  printf("publisher thread %d reading %s\n", arg->index, (char*)arg->filename);
  printf("In function Publisher thread id = %ld\n", pthread_self());
  pthread_mutex_unlock(&lock); 
}

void *Cleanup(void * arg){
  pthread_mutex_lock(&lock);
  pthread_cond_wait(&cond1, &lock);
  printf("%s\n", (char*)arg);
  printf("In function Cleanup thread id = %ld\n", pthread_self());
  pthread_mutex_unlock(&lock); 
}

int main(int argc, char *argv[]){

  //// Initialize variables
  // pthread_t subscriber, publisher, cleanup;
  // char * args = "Hello This Is Subscriber\n";
  // char * argp = "Hello This Is Publisher!\n";
  // char * argcl = "Hello This Is Cleanup!\n";
  // int subscriber_ret = pthread_create(&subscriber, NULL, Subscriber, (void*)args);
  // int publisher_ret = pthread_create(&publisher, NULL, Publisher, (void*)argp);
  // int cleanup_ret = pthread_create(&cleanup, NULL, Cleanup, (void*)argcl);

  // sleep(4);
  // int check = 0;
  // for(int i = 0; i < 20; i++)
  //   printf("*****");
  // printf("\n");
  // pthread_cond_broadcast(&cond1);
  // //Join Threads
  // check += pthread_join(subscriber, NULL);
  // check += pthread_join(publisher, NULL);
  // check += pthread_join(cleanup, NULL);

  // if(check == 0)
  //   printf("\n\nALL THREADS JOINED\n");

  // for(int i = 0; i < 20; i++)
  //   printf("*****");
  // printf("\n");

  for(int i=0; i< argc; i++){
  	fprintf(stdout,"Argument %d:%s\n",i,argv[i]);
  }

  if(argc !=2 )
  {
  	fprintf(stderr,"ERROR: invalid arguments. Usage: ./<prgm> <testfilename>\n");
  	return -1;
  }

  struct FileLines* Lines = NULL;

  Lines = LoadAFile(argv[1]);

  char * line = NULL;

  printf("%d\n", Lines->LineCount);

  struct line_arguments **arguments = malloc(Lines->LineCount * sizeof(struct line_arguments *));

  for(int i = 0; i < Lines->LineCount; i++){
  	arguments[i] = malloc(sizeof(struct line_arguments));
  }

  for(int i = 0; i < Lines->LineCount; i++){
  	arguments[i]->args = malloc(20*sizeof(char*));

  	line = strtok(Lines->Lines[i]," \n");

  	for(int j = 0; line; j++){
  		if(line != NULL){
  			arguments[i]->args[j] = malloc(strlen(line)+1);
  			strcpy(arguments[i]->args[j], line);
  		}
  		line = strtok(NULL, " \n");
  	}
  }

  for(int i = 0; i < Lines->LineCount; i++){
  	printf("\nLine %d: ", i);
  	for(int j = 0; j < 5; j++){
  		if(arguments[i]->args[j] != NULL)
  			printf("\t Arg %d: %s", j, arguments[i]->args[j]);
  	}
  }

  printf("\n");

  pthread_t publisher_threads[THREADSIZE];
  pthread_t subscriber_threads[THREADSIZE];
  pthread_t cleanup;
  TSBoundedQueue *topic_queues[QUEUESIZE];
  char *topicnames[QUEUESIZE];
  char *publishernames[THREADSIZE];
  char *subscribernames[THREADSIZE];
  int topic_count = 0;
  int publisher_count = 0;
  int subscriber_count = 0;

  for(int i = 0; i < Lines->LineCount; i++){
  	if(strcmp(arguments[i]->args[0],"start") == 0)
  		break;
  	else if(strcmp(arguments[i]->args[0],"create") == 0){
  		if(arguments[i]->args[1] == "topic"){
  			int size = p1atoi(arguments[i]->args[4]);
  			TSBoundedQueue *topic_queue = MallocTopicQueue(size);
  			topic_queues[topic_count] = topic_queue;
  			topicnames[topic_count] = arguments[i]->args[3];
  			topic_count += 1;
  		}
  		else if(strcmp(arguments[i]->args[1],"publisher") == 0){
  			struct arg_struct *args = malloc(sizeof(struct arg_struct*));
  			args->filename = arguments[i]->args[2];
  			args->index = publisher_count;
  			publishernames[publisher_count] = arguments[i]->args[2];
  			pthread_create(&publisher_threads[publisher_count], NULL, Publisher, (void*)args);
  			publisher_count += 1;
  		}
  		else if(strcmp(arguments[i]->args[1],"subscriber") == 0){
  			struct arg_struct *args = malloc(sizeof(struct arg_struct*));
  			args->filename = arguments[i]->args[2];
  			args->index = subscriber_count;
  			subscribernames[subscriber_count] = arguments[i]->args[2];
  			pthread_create(&subscriber_threads[subscriber_count], NULL, Subscriber, (void*)args);
  			subscriber_count += 1;
  		}
  	}
  	else if(strcmp(arguments[i]->args[0],"Delta") == 0){
  		DELTA = p1atoi(arguments[i]->args[1]);
  		printf("DELTA %d", DELTA);
  	}
  	else{
  		printf("%s\n", arguments[i]->args[1]);
  		if(strcmp(arguments[i]->args[1],"topics") == 0){
  			for(int i = 0; i < topic_count; i++){
  				printf("topic %d %d\n", i, topic_queues[i]->queue->size);
  			}
  		}
  		else if(strcmp(arguments[i]->args[1],"publishers") == 0){
  			for(int i = 0; i < publisher_count; i++){
  				printf("publishers thread %d %s\n", i, publishernames[i]);
  			}
  		}
  		else if(strcmp(arguments[i]->args[1],"subscribers") == 0){
  			for(int i = 0; i < subscriber_count; i++){
  				printf("subscriber thread %d %s\n", i, subscribernames[i]);
  			}
  		}
  	}
  }

  char *args = "Dequeue Thread\n";
  pthread_create(&cleanup, NULL, Cleanup, (void*)args);
  int check = 0;
  sleep(5);
  for(int i = 0; i < 20; i++)
    printf("*****");
  printf("\n");

  pthread_cond_broadcast(&cond1);

  for(int i = 0; i < publisher_count; i++){
  	check += pthread_join(publisher_threads[i], NULL);
  	if(check == 0)
  		printf("publisher thread %d exited\n", i);
  	else
  		printf("publisher thread %d failed to exit\n", i);
  }

  for(int i = 0; i < subscriber_count; i++){
  	check += pthread_join(subscriber_threads[i], NULL);
  	if(check == 0)
  		printf("subscriber thread %d exited\n", i);
  	else
  		printf("subscriber thread %d failed to exit\n", i);
  }

  check += pthread_join(cleanup, NULL);
  	if(check == 0)
  		printf("Dequeue thread exited\n");
  for(int i = 0; i < 20; i++)
  printf("*****");
  printf("\n");
  /*
  PARSE THE LINES IN Lines->Lines[i];
  */

  FreeFile(Lines);

  return 0;
}