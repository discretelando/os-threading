create topic 0 basketball 25
create topic 1 outdoor 50
create topic 2 cats 10
create topic 3 boom 50
create topic 4 baby 10
query topics
create publisher sports_publisher.txt
create publisher cats_and_basketball_publisher.txt
create publisher outdoor_publisher.txt
create publisher dogs_publisher.txt
create publisher outdoor_woos_shack_publisher.txt
query publishers
create subscriber basketball_reader.txt
create subscriber everything_reader.txt
create subscriber rider_reader.txt
create subscriber keeanu_reader.txt
query subscribers
Delta 100
start