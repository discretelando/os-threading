/*
This file outlines the structures needed by our thread queue to 
perform its tasks
*/
#ifndef THREAD_STRUCTS_H
#define THREAD_STRUCTS_H
#include "thread_safe_bounded_queue.h"
#include <sys/time.h>
#define QUACKSIZE 500
#define	CAPTIONSIZE 1000


typedef struct thread_safe_topic_entry Entry;

typedef struct line_arguments Argument;

typedef struct timeval Timeval;

struct thread_safe_topic_entry{
	int entrynum;
	Timeval *timestamp;
	int pubID;
	char photoURL[QUACKSIZE]; //url to each photo
	char photoCaption[CAPTIONSIZE]; //caption that accompanies each photo
};

struct line_arguments{
	int count;
	char **args;
};

TSBoundedQueue *MallocTopicQueue(long size);

long long TryAddEntry(TSBoundedQueue *queue,void *item); 

int Thread_TryDequeue(TSBoundedQueue *queue,long long id);

long long Thread_GetFront(TSBoundedQueue *queue);

long long Thread_GetBack(TSBoundedQueue *queue);

int Thread_GetCount(TSBoundedQueue *queue);

int Thread_IsIdValid(TSBoundedQueue *queue,long long id);

void *Thread_GetItem(TSBoundedQueue *queue,long long id);

int Thread_IsFull(TSBoundedQueue *queue);

int Thread_IsEmpty(TSBoundedQueue *queue);

void Thread_FreeBoundedQueue(TSBoundedQueue *queue);

Entry *MakeEntry();

#endif
